﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderBarra : MonoBehaviour
{
    bool Q, W, E, R, T;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Q = Input.GetKeyDown(KeyCode.Q);
        W = Input.GetKeyDown(KeyCode.W);
        E = Input.GetKeyDown(KeyCode.E);
        R = Input.GetKeyDown(KeyCode.R);
        T = Input.GetKeyDown(KeyCode.T);
    }

    private void OnTriggerStay(Collider other)
    {
        if (Q)
        {
            if(tag == "q")
            {   
                Destroy(other.gameObject);
                GameManager.instance.puntos++; 
            }
        }
        if (W)
        {
            if (tag == "w")
            {
                Destroy(other.gameObject);
                GameManager.instance.puntos++;
            }
        }
        if (E)
        {
            if (tag == "e")
            {
                Destroy(other.gameObject);
                GameManager.instance.puntos++;
            }
        }
        if (R)
        {
            if (tag == "r")
            {
                Destroy(other.gameObject);
                GameManager.instance.puntos++;
            }
        }
        if (T)
        {
            if (tag == "t")
            {
                Destroy(other.gameObject);
                GameManager.instance.puntos++;
            }
        }
    }
}
