﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool levelFinished = false;
    public static GameManager instance;
    public GameObject[] railes;
    public GameObject Pelota;
    public int puntos = 0;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else Destroy(this);

    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator Timer()
    {
        int contadorDeSpawn = 0;
        for (int i = 0; i < railes.Length; i++)
        {
            if (Random.Range(0, 6) == 5)
            {
                contadorDeSpawn++;
                if (contadorDeSpawn <= 4)
                {
                    generador(railes[i]);
                }
            }
        }
        if (contadorDeSpawn == 0)
        {
            generador(railes[0]);
        }
        
        yield return new WaitForSeconds(0.75f);
        Debug.Log("Llevas " + puntos + " puntos!");
        if (levelFinished)
        {

        }

        else StartCoroutine(Timer());
    }

    public void generador(GameObject Rail)
    {
        {
            Instantiate(Pelota, new Vector3(Rail.transform.position.x, 2.1f, 27.58f), transform.rotation);
        }
    }
}
